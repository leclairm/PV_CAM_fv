from setuptools import setup, find_packages
from distutils.extension import Extension
import numpy as np

module1 = [Extension("PV_CAM_fv.DGrid_PV",
                     sources=["PV_CAM_fv/DGrid_PV.c"],
                     extra_compile_args=["-O3"],
                     include_dirs=[np.get_include(), "."])]

setup(name="PV_CAM_fv",
      version="0.2",
      description="Potential vorticity for CAM fv dycore using the native D grid output",
      author="Matthieu Leclair",
      author_email="matthieu.leclair@env.ethz.ch",
      url="https://git.iac.ethz.ch/leclairm/pv_cam_fv-dycore",
      install_requires=['netCDF4', 'numpy'],
      ext_modules=module1,
      entry_points={
          'console_scripts': ['pv_cam_fv = PV_CAM_fv.console_scripts:compute_pv',
                              'vapv_cam_fv = PV_CAM_fv.console_scripts:compute_vapv']
      },
      packages=['PV_CAM_fv'],
      package_data={'PV_CAM_fv': ['*.pxd', '*.pyx']}
)
