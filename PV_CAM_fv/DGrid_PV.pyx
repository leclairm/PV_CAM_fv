#cython: language_level=3, boundscheck=False, wraparound=False, cdivision=True

from os.path import samefile, isfile
import numpy as np
import netCDF4 as nc
from time import time
from sys import stdout

cimport numpy as np

# Data type
# =========
DTYPE = np.float32

# Physical constants
# ==================
cdef DTYPE_t R = 6.37e6
cdef DTYPE_t rcp = 287.0 / 1003.0
cdef DTYPE_t ge6 = 9.807e6   # 1.e6 factor for PV units

# netcdf fill value
fill_value = DTYPE(-2**127)

# ==================================================
#             Core PV computing function
# ==================================================

cdef void compute_DGrid_pv(
    DTYPE_t[:,:,::1] US,
    DTYPE_t[:,:,::1] VS,
    DTYPE_t[:,:,::1] TH,
    DTYPE_t[:,:,::1] P,
    DTYPE_t[::1] f,
    DTYPE_t[::1] d_S_1,
    DTYPE_t[::1] cos_lat_d_phi_1,
    DTYPE_t[::1] cos_slat_d_phi,
    DTYPE_t[::1] alpha_N_d_lbda,
    DTYPE_t[::1] alpha_S_d_lbda,
    DTYPE_t d_lbda,
    size_t k_min,
    size_t k_max,
    DTYPE_t[:,:,::1] PV,
    DTYPE_t[:,::1] PV_cor,
    DTYPE_t[:,:,::1] dP,
    DTYPE_t[:,:,::1] dTH_dP,
    DTYPE_t[:,:,::1] dUS_dP,
    DTYPE_t[:,:,::1] dVS_dP) nogil:
    """Compute potential vorticity based on the native D-Grid CAM output for a
     single time record

        Input fileds
        ============
        We assume the CAM tracer grid - centers of the D-Grid cells - has a
        shape of (n_lev, n_lat, n_lon).
        US              : Staggered zonal Velocity [m/s]
                          float32 [n_lev, n_lat-1, n_lon]
        VS              : Staggered meridionnal Velocity [m/s]
                          float32 [n_lev, n_lat, n_lon]
        TH              : Potential Temperature [K]
                          float32 [n_lev, n_lat, n_lon]
        P               : Pressure [Pa]
                          float32 [n_lev, n_lat, n_lon]
        f               : Coriolis Parameter [s^-1]
                          float32 [n_lat]
                          -> at the cell center
        d_S_1           : Inverse Cell horizontal Area normalized by Earth's Radius [m^-1]
                          float32 [n_lat]
        cos_lat_d_phi_1 : Inverse Latitude Cosine x delta Longitude []
                          float32 [n_lat]
                          -> at the cell center, uninitialized at the poles
        cos_slat_d_phi  : Staggered Latitude Cosine x delta Longitude []
                          float32 [n_lat-1]
                          -> at staggered "u-points"
        alpha_N_d_lbda  : proportion of the cell area "norh of P-point" / delta latitude []
                          float32 [n_lat]
        alpha_S_d_lbda  : proportion of the cell area "south of P-point" / delta latitude []
                          = d_lbda - alpha_N by definition
                          float32 [n_lat]
        d_lbda          : Latitude Resolution [rad]
                          float32 scalar
        k_min           : First vertical Index where PV has to be computed
                          python unsigned index
        k_max           : Last vertical Index where PV has to be computed
                          python unsigned index

        Output field
        ============
        PV       : Potential Vorticity
                   float32 [n_lev, n_lat, n_lon]

        Temporary arrays
        ================
        These are "workspace" variables but given as input to avoid local declaration/allocation
        PV_cor    : Correction term due to non alignment between computational
                    and isentropic surfaces
                    float32 [n_lat, n_lon]
        dP        : pressure delta
                    float32 [2, n_lat, n_lon]
        dTH_dP    : potential temperature derivative with respect to pressure
                    float32 [2, n_lat, n_lon]
        dUS_dP    : zonal velocity derivative with respect to pressure
                    float32 [2, n_lat-1, n_lon]
        dVS_dP    : meridionnal velocity derivative with respect to pressure
                    float32 [2, n_lat, n_lon]
    """

    # Declarations
    # ============
    # Dimensions
    cdef size_t n_lev = P.shape[0], n_lat = P.shape[1], n_lon = P.shape[2]
    # "Calssical" Indices
    cdef size_t k, j, i
    # Caution, trick! Indices for upper and lower vertical derivatives/deltas
    # switch between 0 and 1, avoids moving data from 1 to 0
    cdef size_t kp = 1   # higher pressure (larger k)
    cdef size_t km = 0   # lower pressure (smaller k)
    # temporary scalar
    cdef DTYPE_t tmp, alpha
    cdef DTYPE_t scal_0_0 = 0.0, scal_1_0 = 1.0, scal_2_0 = 2.0

    for k in range(k_min, k_max+1):
        # initialize circulation
        # ======================
        # Caution, trick! Only initializing j=0 is necessary.
        # Other levels are initialized during treatment of zonal velocity
        # (See contribute vs. initialize comments bellow)
        PV[k,0,:] = scal_0_0
        PV_cor[0,:] = scal_0_0

        # Store P intervals
        # =================
        if k < n_lev-1:
            for j in range(n_lat):
                for i in range(n_lon):
                    dP[kp,j,i] = P[k+1,j,i] - P[k,j,i]

        # Zonal velocity
        # ==============
        # First level
        # ~~~~~~~~~~~
        if k == 0:
            for j in range(n_lat-1):
                for i in range(n_lon):
                    # Circulations on computational surfaces
                    # --------------------------------------
                    tmp = cos_slat_d_phi[j] * US[k,j,i]
                    PV[k,j,i] -= tmp   # contribute
                    PV[k,j+1,i] = tmp   # initialize
                    # Correction term
                    # ---------------
                    dUS_dP[kp,j,i] = scal_2_0 * (US[k+1,j,i] - US[k,j,i]) / (dP[kp,j,i] + dP[kp,j+1,i])
                    tmp = (TH[k,j+1,i] - TH[k,j,i]) * dUS_dP[kp,j,i]
                    PV_cor[j,i] += alpha_N_d_lbda[j] * tmp   # contribute
                    PV_cor[j+1,i] = alpha_S_d_lbda[j+1] * tmp   #initialize
        # Last level
        # ~~~~~~~~~~
        elif k == n_lev-1:
            for j in range(n_lat-1):
                for i in range(n_lon):
                    # Circulations on computational surfaces
                    # --------------------------------------
                    tmp = cos_slat_d_phi[j] * US[k,j,i]
                    PV[k,j,i] -= tmp   # contribute
                    PV[k,j+1,i] = tmp   # initialize
                    # Correction term
                    # ---------------
                    tmp = (TH[k,j+1,i] - TH[k,j,i]) * dUS_dP[km,j,i]
                    PV_cor[j,i] += alpha_N_d_lbda[j] * tmp   # contribute
                    PV_cor[j+1,i] = alpha_S_d_lbda[j+1] * tmp   #initialize
        # Inner levels
        # ~~~~~~~~~~~~
        else:
            for j in range(n_lat-1):
                for i in range(n_lon):
                    # Circulations on computational surfaces
                    # --------------------------------------
                    tmp = cos_slat_d_phi[j] * US[k,j,i]
                    PV[k,j,i] -= tmp   # contribute
                    PV[k,j+1,i] = tmp   # initialize
                    # Correction term
                    # ---------------
                    dUS_dP[kp,j,i] = scal_2_0 * (US[k+1,j,i] - US[k,j,i]) / (dP[kp,j,i] + dP[kp,j+1,i])
                    tmp = dP[km,j,i] + dP[km,j+1,i]
                    alpha = tmp / (tmp + dP[kp,j,i] + dP[kp,j+1,i])
                    tmp = (alpha * dUS_dP[kp,j,i] + (scal_1_0-alpha) * dUS_dP[km,j,i]) \
                          * (TH[k,j+1,i] - TH[k,j,i])
                    PV_cor[j,i] += alpha_N_d_lbda[j] * tmp   # contribute
                    PV_cor[j+1,i] = alpha_S_d_lbda[j+1] * tmp   #initialize

        # Meridionnal velocity
        # ====================
        # First index is treated separately to handle zonal periodicity
        # First level
        # ~~~~~~~~~~~
        if k == 0:
            for j in range(1,n_lat-1):
                i = 0
                # Circulations on computational surfaces
                # --------------------------------------
                tmp = d_lbda * VS[k,j,i]
                PV[k,j,i] -= tmp
                PV[k,j,n_lon-1] += tmp
                # Correction term
                # ---------------
                # Compensation between missing factor 2.0 in dVS_dP followed by ignored division by 2.0
                dVS_dP[kp,j,i] = (VS[k+1,j,i] - VS[k,j,i]) / (dP[kp,j,i] + dP[kp,j,n_lon-1])
                tmp = (TH[k,j,i] - TH[k,j,n_lon-1]) * dVS_dP[kp,j,i] * cos_lat_d_phi_1[j]
                PV_cor[j,i] -= tmp
                PV_cor[j,n_lon-1] -= tmp
                for i in range(1,n_lon):
                    # Circulations on computational surfaces
                    # --------------------------------------
                    tmp = d_lbda * VS[k,j,i]
                    PV[k,j,i] -= tmp
                    PV[k,j,i-1] += tmp
                    # Correction term
                    # ---------------
                    # Compensation between missing factor 2.0 in dVS_dP followed by ignored division by 2.0
                    dVS_dP[kp,j,i] = (VS[k+1,j,i] - VS[k,j,i]) / (dP[kp,j,i] + dP[kp,j,i-1])
                    tmp = (TH[k,j,i] - TH[k,j,i-1]) * dVS_dP[kp,j,i] * cos_lat_d_phi_1[j]
                    PV_cor[j,i] -= tmp
                    PV_cor[j,i-1] -= tmp
        # Last level
        # ~~~~~~~~~~
        elif k == n_lev-1:
            for j in range(1,n_lat-1):
                i = 0
                # Circulations on computational surfaces
                # --------------------------------------
                tmp = d_lbda * VS[k,j,i]
                PV[k,j,i] -= tmp
                PV[k,j,n_lon-1] += tmp
                # Correction term
                # ---------------
                tmp = (TH[k,j,i] - TH[k,j,n_lon-1]) * dVS_dP[km,j,i] * cos_lat_d_phi_1[j]
                PV_cor[j,i] -= tmp
                PV_cor[j,n_lon-1] -= tmp
                for i in range(1,n_lon):
                    # Circulations on computational surfaces
                    # --------------------------------------
                    tmp = d_lbda * VS[k,j,i]
                    PV[k,j,i] -= tmp
                    PV[k,j,i-1] += tmp
                    # Correction term
                    # ---------------
                    tmp = (TH[k,j,i] - TH[k,j,i-1]) * dVS_dP[km,j,i] * cos_lat_d_phi_1[j]
                    PV_cor[j,i] -= tmp
                    PV_cor[j,i-1] -= tmp
        # Inner levels
        # ~~~~~~~~~~~~
        else:
            for j in range(1,n_lat-1):
                i = 0
                # Circulations on computational surfaces
                # --------------------------------------
                tmp = d_lbda * VS[k,j,i]
                PV[k,j,i] -= tmp
                PV[k,j,n_lon-1] += tmp
                # Correction term
                # ---------------
                # Compensation between missing factor 2.0 in dVS_dP followed by ignored division by 2.0
                dVS_dP[kp,j,i] = (VS[k+1,j,i] - VS[k,j,i]) / (dP[kp,j,i] + dP[kp,j,n_lon-1])
                tmp = dP[km,j,i] + dP[km,j,n_lon-1]
                alpha = tmp / (tmp + dP[kp,j,i] + dP[kp,j,n_lon-1])
                tmp = (alpha * dVS_dP[kp,j,i] + (scal_1_0-alpha) * dVS_dP[km,j,i]) \
                      * (TH[k,j,i] - TH[k,j,n_lon-1]) * cos_lat_d_phi_1[j]
                PV_cor[j,i] -= tmp
                PV_cor[j,n_lon-1] -= tmp
                for i in range(1,n_lon):
                    # Circulations on computational surfaces
                    # --------------------------------------
                    tmp = d_lbda * VS[k,j,i]
                    PV[k,j,i] -= tmp
                    PV[k,j,i-1] += tmp
                    # Correction term
                    # ---------------
                    # Compensation between missing factor 2.0 in dVS_dP followed by ignored division by 2.0
                    dVS_dP[kp,j,i] = (VS[k+1,j,i] - VS[k,j,i]) / (dP[kp,j,i] + dP[kp,j,i-1])
                    tmp = dP[km,j,i] + dP[km,j,i-1]
                    alpha = tmp / (tmp + dP[kp,j,i] + dP[kp,j,i-1])
                    tmp = (alpha * dVS_dP[kp,j,i] + (scal_1_0-alpha) * dVS_dP[km,j,i]) \
                          * (TH[k,j,i] - TH[k,j,i-1]) * cos_lat_d_phi_1[j]
                    PV_cor[j,i] -= tmp
                    PV_cor[j,i-1] -= tmp

        # Finalize circulation and correction at the poles
        # ================================================
        for i in range(1,n_lon):
            PV[k,0,0] += PV[k,0,i]
            PV[k,n_lat-1,0] += PV[k,n_lat-1,i]
            PV_cor[0,0] += PV_cor[0,i]
            PV_cor[n_lat-1,0] += PV_cor[n_lat-1,i]
        PV[k,0,1:] = PV[k,0,0]
        PV[k,n_lat-1,1:] = PV[k,n_lat-1,0]
        PV_cor[0,1:] = PV_cor[0,0]
        PV_cor[n_lat-1,1:] = PV_cor[n_lat-1,0]

        # Compute potential vorticity
        # ===========================
        # First level
        # ~~~~~~~~~~~
        if k == 0:
            for j in range(n_lat):
                for i in range(n_lon):
                    dTH_dP[kp,j,i] = (TH[k+1,j,i] - TH[k,j,i]) / dP[kp,j,i]
                    # Compute PV
                    PV[k,j,i] = -ge6 * ((f[j] + PV[k,j,i]*d_S_1[j]) * dTH_dP[kp,j,i] + PV_cor[j,i]/R)
        # Last level
        # ~~~~~~~~~~
        elif k == n_lev-1:
            for j in range(n_lat):
                for i in range(n_lon):
                    # Compute PV
                    PV[k,j,i] = -ge6 * ((f[j] + PV[k,j,i]*d_S_1[j]) * dTH_dP[km,j,i] + PV_cor[j,i]/R)
        # Inner levels
        # ~~~~~~~~~~~~
        else:
            for j in range(n_lat):
                for i in range(n_lon):
                    # Store P vertical intervals
                    dTH_dP[kp,j,i] = (TH[k+1,j,i] - TH[k,j,i]) / dP[kp,j,i]
                    # Compute PV
                    alpha = dP[km,j,i] / (dP[kp,j,i] + dP[km,j,i])
                    tmp = alpha * dTH_dP[kp,j,i] + (scal_1_0 - alpha) * dTH_dP[km,j,i]
                    PV[k,j,i] = -ge6 * ((f[j] + PV[k,j,i]*d_S_1[j]) * tmp + PV_cor[j,i]/R)


        # Switch kp and km for next iteration
        # ===================================
        kp = 1 - kp
        km = 1 - km

# ==================================================
#     Pressure intervals for vertical integration
# ==================================================

cdef void compute_P_intervals(
    DTYPE_t[::1] P0_hyai,
    DTYPE_t[:,::1] PS,
    DTYPE_t[::1] hybi,
    DTYPE_t P_min,
    DTYPE_t P_max,
    DTYPE_t[:,:,::1] delta_P) nogil:
    """Compute pressure intervals for PV integration

        Input fields
        ============
        Assuming the CAM tracer grid - centers of the D-Grid cells - has a shape
        of (n_lev, n_lat, n_lon),
        P0_hyai : reference pressure times hybrid A coefficient at layer interfaces
                  float32 [n_lev+1]
        PS      : Surface pressure [Pa]
                  float32 [n_lat, n_lon]
        hybi    : hybrid B coefficient at layer interfaces
                  float32 [n_lev+1]
        P_min   : Lower bound of pressure range for PV integration
                  float32 scalar
        P_max   : Upper bound of pressure range for PV integration
                  float32 scalar
        delta_P : pressure intervals for PV integration taking partial
                  layers into account
                  float32[n_lev,n_lat,n_lon]
                  input field but updated in in this function

    """

    # Get grid dimensions (pressure points, not intermediate levels)
    # -------------------
    cdef int n_lon, n_lat, n_lev
    n_lon = PS.shape[1]
    n_lat = PS.shape[0]
    n_lev = hybi.shape[0] - 1

    # Compute pressure intervals
    # --------------------------
    cdef size_t k ,j, i
    cdef DTYPE_t P_p, P_m
    cdef DTYPE_t scal_0_0 = 0.0
    for k in range(n_lev):
        for j in range(n_lat):
            for i in range(n_lon):
                P_m = P0_hyai[k] + PS[j,i] * hybi[k]
                P_p = P0_hyai[k+1] + PS[j,i] * hybi[k+1]
                if P_p < P_min:
                    delta_P[k,j,i] = scal_0_0
                elif P_m < P_min:
                    delta_P[k,j,i] = P_p - P_min
                elif P_p < P_max:
                    delta_P[k,j,i] = P_p - P_m
                elif P_m < P_max:
                    delta_P[k,j,i] = P_max - P_m
                else:
                    delta_P[k,j,i] = scal_0_0

# ==================================================
#           min and max vertical indices
# ==================================================

cdef size_t get_min_index(DTYPE_t[:,:,::1] dP) nogil:

    cdef size_t k, j ,i, k_found=-1
    cdef size_t n_lev = dP.shape[0], n_lat = dP.shape[1], n_lon = dP.shape[2]
    cdef bint found = False
    cdef DTYPE_t scal_0_0 = 0.0

    for k in range(n_lev):
        if found:
            break
        for j in range(n_lat):
            if found:
                break
            for i in range(n_lon):
                found = dP[k,j,i] > scal_0_0
                if found:
                    k_found = k
                    break
    return k_found

cdef size_t get_max_index(DTYPE_t[:,:,::1] dP) nogil:

    cdef size_t k, j ,i, k_found=-1
    cdef size_t n_lev = dP.shape[0], n_lat = dP.shape[1], n_lon = dP.shape[2]
    cdef bint found = False
    cdef DTYPE_t scal_0_0 = 0.0

    for k in range(n_lev):
        if found:
            break
        for j in range(n_lat):
            if found:
                break
            for i in range(n_lon):
                found =  dP[n_lev-1-k,j,i] > scal_0_0
                if found:
                    k_found = n_lev-1-k
                    break
    return k_found

# ==================================================
#         Pressure and potential temperature       
# ==================================================

cdef void compute_P_TH(
    DTYPE_t P0,
    DTYPE_t[::1] P0_a,
    DTYPE_t[:,::1] PS,
    DTYPE_t[::1] b,
    size_t k0,
    size_t k1,
    DTYPE_t[:,:,::1] P,
    DTYPE_t[:,:,::1] TH) nogil:

    cdef size_t k
    cdef size_t n_lat = PS.shape[0]
    cdef size_t n_lon = PS.shape[1]

    cdef DTYPE_t P_tmp

    for k in range(k0,k1):
        for j in range(n_lat):
            for i in range(n_lon):
                P_tmp = P0_a[k] + PS[j,i] * b[k]
                P[k,j,i] = P_tmp
                TH[k,j,i] = TH[k,j,i] * (P0 / P_tmp) ** rcp

# ==================================================
#                  Vertical average
# ==================================================

cdef void compute_va(
    DTYPE_t[:,:,::1] PV,
    DTYPE_t[:,:,::1] dP,
    DTYPE_t[:,::1] APV,
    DTYPE_t[:,::1] dP_sum,
    size_t k_min,
    size_t k_max) nogil:

    cdef size_t n_lat = PV.shape[1]
    cdef size_t n_lon = PV.shape[2]
    cdef size_t k, j, i

    for j in range(n_lat):
        for i in range(n_lon):
            APV[j,i] = PV[k_min,j,i] * dP[k_min,j,i]
            dP_sum[j,i] = dP[k_min,j,i]

    for k in range(k_min+1, k_max+1):
        for j in range(n_lat):
            for i in range(n_lon):
                APV[j,i] += PV[k,j,i] * dP[k,j,i]
                dP_sum[j,i] += dP[k,j,i]

    for j in range(n_lat):
        for i in range(n_lon):
            APV[j,i] = APV[j,i] / dP_sum[j,i]

# ==================================================
#  Extract data from file and use core PV function
# ==================================================

def compute_PV_from_file(in_file, out_file=None,
                         v_avg=False, P_min=0, P_max=0,
                         n_start=None, n_end=None,
                         monitor_level=1):
    """Compute PV from file.

        Input fields
        ============
        in_file       : Input netcdf CAM file. Must contain US, VS, T (plus classical
                        grid info)
        out_file      : netcdf file to write to
        v_avg         : Perform vertical averaging of PV between P_min and P_max
                        logical
        P_min         : Lower bound of pressure range for vertical integration in Pa.
                        Only effective if v_avg is true
                        float32 scalar
                        default 0 (whole air column)
        P_max         : Upper bound of pressure range for vertical integration in Pa.
                        Only effective if v_avg is true
                        float32 scalar
                        default 0 (whole air column)
        n_start       : Index of first time slice to treat
                        default None, start with first index
        n_end         : Index of the las time slice to treat
                        default None, finish with last index
        monitor_level : level of time monitoring in different sections
                        < 0 no monitoring
                          1 total time and progress
                        > 1 detailed time in different sections
    """

    # Timing
    # ======
    if monitor_level > 0:
        start_time_glob = time()
    if monitor_level > 1:
        times = {'Init':0.0, 'Read':0.0, 'Write':0.0, 'Process':0.0}

    # Init access to nc files and init output file
    # ============================================
    if monitor_level > 1:
        start_time = time()
    # Check if (A)PV is written to the input file
    if out_file is None:
        same_in_out = True
    elif isfile(out_file):
        same_in_out = samefile(in_file, out_file)
    else:
        same_in_out = False


    # Input
    # -----
    rec_in = nc.Dataset(in_file, mode='a' if same_in_out else 'r')

    US_nc, VS_nc, T_nc, PS_nc = rec_in['US'], rec_in['VS'], rec_in['T'], rec_in['PS']

    n_lev, n_lat, n_lon = T_nc.shape[1], T_nc.shape[2], T_nc.shape[3]

    n_s = 0 if n_start is None else max(0, n_start-1)
    n_e = T_nc.shape[0] if n_end is None else min(n_end, T_nc.shape[0])
    nt = n_e - n_s

    # output
    # ------
    if same_in_out:
        rec_out = rec_in
    else:
        rec_out = nc.Dataset(out_file, mode='w')
        for dim_name in ('lev', 'lat', 'lon'):
            rec_out.createDimension(dim_name, rec_in.dimensions[dim_name].size)
        rec_out.createDimension('time', nt)
        if v_avg:
            var_list = ('lat', 'lon', 'time', 'date', 'datesec')
        else:
            var_list = ('lat', 'lon', 'time', 'date', 'datesec', 'PS', 'P0', 'hyam', 'hybm')
        for var_name in var_list:
            var_in = rec_in[var_name]
            var_out = rec_out.createVariable(var_name, var_in.dtype,
                                             dimensions=var_in.dimensions)
            if 'time' in var_in.dimensions:
                var_out[:] = var_in[n_s:n_e]
            else:
                var_out[:] = var_in[:]
            for att in var_in.ncattrs():
                var_out.setncattr(att, var_in.getncattr(att))

    # Init PV variables
    # =================
    if v_avg:
        if 'APV' not in rec_out.variables:
            APV_nc = rec_out.createVariable('APV', DTYPE,
                                             dimensions=('time', 'lat', 'lon'),
                                             fill_value=fill_value)
            APV_nc.setncattr("long_name", "Vertically Averaged Potential Vorticity")
            APV_nc.setncattr("Pressure interval", '[{:.2f}, {:.2f}]'.format(P_min, P_max))
            APV_nc.setncattr("units", 'PVU')
        else:
            APV_nc = rec_out['APV']
    else:
        if 'PV' not in rec_out.variables:
            PV_nc = rec_out.createVariable('PV', DTYPE,
                                           dimensions=('time', 'lev', 'lat', 'lon'),
                                           fill_value=fill_value)
            PV_nc.setncattr("long_name", "Potential Vorticity")
            PV_nc.setncattr("units", 'PVU')
        else:
            PV_nc = rec_out['PV']


    # Init in-memory variables
    # ========================

    # variables defining the grid
    # ---------------------------
    P0 = DTYPE(rec_in['P0'][0])
    pmin = DTYPE(P_min)
    pmax = DTYPE(P_max)
    deg_to_rad = DTYPE(np.pi/180.0)
    P0_hyai = P0 * rec_in['hyai'][:].astype(DTYPE, copy=False)
    P0_hyam = P0 * rec_in['hyam'][:].astype(DTYPE, copy=False)
    hybi = rec_in['hybi'][:].astype(DTYPE, copy=False)
    hybm = rec_in['hybm'][:].astype(DTYPE, copy=False)
    lbda = rec_in['lat'][:].astype(DTYPE, copy=False) * deg_to_rad
    slbda = rec_in['slat'][:].astype(DTYPE, copy=False) * deg_to_rad
    phi = rec_in['lon'][:].astype(DTYPE, copy=False) * deg_to_rad

    # Variables derived from geometry for PV computation
    # --------------------------------------------------
    # Twice omega for practical reasons
    omega_2 = DTYPE(2.0 * (2.0 * np.pi) / (23.0 * 3600.0 + 56.0 * 60.0 + 4.1))
    d_lbda = lbda[1] - lbda[0]
    d_phi = phi[1] - phi[0]
    f = omega_2 * np.sin(lbda[:])
    cos_slat_d_phi = np.cos(slbda[:]) * d_phi
    cos_lat_d_phi_1 = np.empty([n_lat], dtype=DTYPE)
    cos_lat_d_phi_1[1:n_lat-1] = 1.0 / (np.cos(lbda[1:n_lat-1]) * d_phi)
    # Inverse cell area normalized by earth radius
    d_S_1 = np.empty([n_lat], dtype=DTYPE)
    d_S_1[1:n_lat-1] = R * d_phi * np.diff(np.sin(slbda))   # regular cells
    d_S_1[0] = 2 * np.pi * R * (1.0 - np.cos(0.5*d_lbda))   # south pole
    d_S_1[n_lat-1] = 2 * np.pi * R * (1.0 - np.cos(0.5*d_lbda))   # north pole
    d_S_1[:] = 1.0 / d_S_1[:]   # invert
    # Proportion of cell area northern and southern of "P-point" divided by d_lbda
    alpha_N_d_lbda = np.empty([n_lat], dtype=DTYPE)
    alpha_S_d_lbda = np.empty([n_lat], dtype=DTYPE)
    alpha_N_d_lbda[1:n_lat-1] = (np.sin(slbda[1:]) - np.sin(lbda[1:n_lat-1])) / np.diff(np.sin(slbda[:]))
    alpha_S_d_lbda[1:n_lat-1] = 1.0 - alpha_N_d_lbda[1:n_lat-1]
    alpha_N_d_lbda[0] = 1.0 / n_lon
    alpha_S_d_lbda[0] = 0.0
    alpha_N_d_lbda[n_lat-1] = 0.0
    alpha_S_d_lbda[n_lat-1] = 1.0 / n_lon
    alpha_N_d_lbda[:] = alpha_N_d_lbda[:] / d_lbda
    alpha_S_d_lbda[:] = alpha_S_d_lbda[:] / d_lbda

    # Time dependant variables
    # ------------------------
    US = np.empty([n_lev, n_lat-1, n_lon], dtype=DTYPE)
    VS = np.empty([n_lev, n_lat, n_lon], dtype=DTYPE)
    TH = np.empty([n_lev, n_lat, n_lon], dtype=DTYPE)
    PS = np.empty([n_lat, n_lon], dtype=DTYPE)
    P = np.empty([n_lev, n_lat, n_lon], dtype=DTYPE)
    PV = np.empty([n_lev, n_lat, n_lon], dtype=DTYPE)
    APV = np.empty([n_lat, n_lon], dtype=DTYPE)
    delta_P = np.empty([n_lev, n_lat, n_lon], dtype=DTYPE)
    delta_P_sum = np.empty([n_lat, n_lon], dtype=DTYPE)

    # Temporary variables (not initialized) used in the core function
    # ---------------------------------------------------------------
    # This enables us not to use the gil at all in the core function
    # and use DTYPE instead of hard coded float
    PV_cor = np.empty([n_lat, n_lon], dtype=DTYPE)
    dP = np.empty([2, n_lat, n_lon], dtype=DTYPE)
    dTH_dP = np.empty([2, n_lat, n_lon], dtype=DTYPE)
    dUS_dP = np.empty([2, n_lat-1, n_lon], dtype=DTYPE)
    dVS_dP = np.empty([2, n_lat, n_lon], dtype=DTYPE)

    if monitor_level > 1:
        times['Init'] += time() - start_time
    # Compute PV
    # ==========
    if monitor_level > 0:
        nt_str = str(nt)
        nd = len(nt_str)
        adv_str = '{{:{:d}d}} / {:s}'.format(nd, nt_str)
        monitor_str = "Treating time step " + adv_str 
        print(monitor_str.format(1), end="")
        back_str = "\b" * len(adv_str.format(1))
        tot_len = len(monitor_str.format(1))
        back_str_tot = "\b" * tot_len

    for kt in range(n_s, n_e):
        kt_out = kt if same_in_out else kt-n_s

        if monitor_level > 0:
            print(back_str + adv_str.format(kt-n_s+1, nt), end="")
            stdout.flush()
        if monitor_level > 1:
            start_time = time()

        # Surface presure
        # ---------------
        PS[:,:] = PS_nc[kt,:,:]
        if monitor_level > 1:
            times['Read'] += time() - start_time
            start_time = time()

        # Pressure intervals and vertical boundary conditions
        # ---------------------------------------------------
        if v_avg:
            compute_P_intervals(P0_hyai, PS, hybi, pmin, pmax, delta_P)
            k_min = get_min_index(delta_P)
            k_max = get_max_index(delta_P)
        else:
            k_min = 0
            k_max = n_lev-1

        k0 = 0 if k_min == 0 else k_min -1
        k1 = n_lev-1 if k_max == n_lev-1 else k_max+1
        k1 += 1   # For slicing
        if monitor_level > 1:
            times['Process'] += time() - start_time
            start_time = time()

        # Staggered velocity components
        # -----------------------------
        US[k0:k1,:,:] = US_nc[kt,k0:k1,:,:]
        VS[k0:k1,:,:] = VS_nc[kt,k0:k1,:,:]

        # Pressure and potential temperature
        # ----------------------------------
        # Read temperature first
        TH[k0:k1,:,:] = T_nc[kt,k0:k1,:,:]
        if monitor_level > 1:
            times['Read'] += time() - start_time
            start_time = time()
        # Then compute pressure and potential temperature
        compute_P_TH(P0, P0_hyam, PS, hybm, k0, k1, P, TH)

        # Compute PV
        # ----------
        compute_DGrid_pv(US, VS, TH, P,
                         f, d_S_1, cos_lat_d_phi_1, cos_slat_d_phi,
                         alpha_N_d_lbda, alpha_S_d_lbda, d_lbda,
                         k_min, k_max,
                         PV,
                         PV_cor, dP, dTH_dP, dUS_dP, dVS_dP)
        if monitor_level > 1:
            times['Process'] += time() - start_time
            start_time = time()

        # Compute vertical average and write to file
        # ------------------------------------------
        if v_avg:
            compute_va(PV, delta_P, APV, delta_P_sum, k_min, k_max)
            if monitor_level > 1:
                times['Process'] += time() - start_time
                start_time = time()
            APV_nc[kt_out,:,:] = APV[:,:]
            if monitor_level > 1:
                times['Write'] += time() - start_time
        else:
            PV_nc[kt_out,k_min:k_max+1,:,:] = PV[k_min:k_max+1,:,:]
            if monitor_level > 1:
                times['Write'] += time() - start_time

    print(back_str_tot, end="")

    # Close nc files
    # ==============
    rec_in.close()
    if not same_in_out:
        rec_out.close()

    # Timing
    # ======
    # Global
    # ------
    if monitor_level > 0:
        m, s = divmod(time()-start_time_glob, 60)
        h, m = divmod(m, 60)
        message = "Finished in {:02d}:{:02d}:{:02d}".format(int(h), int(m), int(s))
        if len(message) < tot_len:
            message_fmt = "{{:{:d}s}}".format(tot_len)
            print(message_fmt.format(message))
        else:
            print(message)
    # Detail
    # ------
    if monitor_level > 1:
        line_fmt = "{:15s} {:02d}:{:02d}:{:02d}   =>   {:5.2f}%"
        dt_tot = sum(times.values())
        print("Detailed time spent in different sections:")
        for sec in ('Init', 'Read', 'Process', 'Write'):
            dt = times[sec]
            m, s = divmod(dt, 60)
            h, m = divmod(m, 60)
            print(line_fmt.format(sec, int(h), int(m), int(s), 100*dt/dt_tot))
