# Data type
cimport numpy as np
ctypedef np.float32_t DTYPE_t

# Pressure and potential temperature
cdef void compute_P_TH(
    DTYPE_t P0,
    DTYPE_t[::1] P0_a,
    DTYPE_t[:,::1] PS,
    DTYPE_t[::1] b,
    size_t k0,
    size_t k1,
    DTYPE_t[:,:,::1] P,
    DTYPE_t[:,:,::1] TH) nogil

# Pressure intervals for vertical integration
cdef void compute_P_intervals(
    DTYPE_t[::1] P0_hyai,
    DTYPE_t[:,::1] PS,
    DTYPE_t[::1] hybi,
    DTYPE_t P_min,
    DTYPE_t P_max,
    DTYPE_t[:,:,::1] delta_P) nogil

# min and max vertical indices
cdef size_t get_min_index(DTYPE_t[:,:,::1] dP) nogil
cdef size_t get_max_index(DTYPE_t[:,:,::1] dP) nogil

# Vertical average
cdef void compute_va(
    DTYPE_t[:,:,::1] PV,
    DTYPE_t[:,:,::1] dP,
    DTYPE_t[:,::1] APV,
    DTYPE_t[:,::1] dP_sum,
    size_t k_min,
    size_t k_max) nogil

# Core PV computing function
cdef void compute_DGrid_pv(
    DTYPE_t[:,:,::1] US,
    DTYPE_t[:,:,::1] VS,
    DTYPE_t[:,:,::1] TH,
    DTYPE_t[:,:,::1] P,
    DTYPE_t[::1] f,
    DTYPE_t[::1] d_S_1,
    DTYPE_t[::1] cos_lat_d_phi_1,
    DTYPE_t[::1] cos_slat_d_phi,
    DTYPE_t[::1] alpha_N_d_lbda,
    DTYPE_t[::1] alpha_S_d_lbda,
    DTYPE_t d_lbda,
    size_t k_min,
    size_t k_max,
    DTYPE_t[:,:,::1] PV,
    DTYPE_t[:,::1] PV_cor,
    DTYPE_t[:,:,::1] dP,
    DTYPE_t[:,:,::1] dTH_dP,
    DTYPE_t[:,:,::1] dUS_dP,
    DTYPE_t[:,:,::1] dVS_dP) nogil
